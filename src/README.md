# Práctica de MVC y Observer

- [Slides de la clase](https://docs.google.com/presentation/d/1sQIg7Q87fHVaH2Fbdv2mq05HhBQdOGUhxLAt1iVPVC0/edit?usp=sharing)
- Implementación de ejemplo (en este repositorio - en progreso)

## Esquema de la clase

1. Hacer un repaso sobre MVC y Observer, haciendo incapié en las variaciones.
2. Presentar la variación que vamos a implementar (es la necesaria para que luego puedan agregar RMI).
3. Plantear el ejercicio y discutir el modelo y la arquitectura.
	- La idea es hacer incapié en la interacción entre las diferentes capas. Por ello, la capa de negocio es sencilla.
	- Tratar de implementar al menos un caso de uso completo donde se pase por todas las capas de la app.
	- Recordar a los alumnos ser cuidadosos en la vista de consola con el stack de llamadas.