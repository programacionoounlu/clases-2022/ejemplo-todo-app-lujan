package ar.edu.unlu.utils;

public interface Observador {
	public void notificar(Object o);
}
