package ar.edu.unlu.utils;

public interface Observable {
	public void agregarObservador(Observador observador);
	public void notificarObsrvadores(Object o);
}
