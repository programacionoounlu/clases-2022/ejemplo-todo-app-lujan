package ar.edu.unlu.todo.controlador;

import java.rmi.RemoteException;

import ar.edu.unlu.rmimvc.cliente.IControladorRemoto;
import ar.edu.unlu.rmimvc.observer.IObservableRemoto;
import ar.edu.unlu.todo.modelo.Eventos;
import ar.edu.unlu.todo.modelo.IToDo;
import ar.edu.unlu.todo.modelo.ToDo;
import ar.edu.unlu.todo.vista.IVista;
import ar.edu.unlu.utils.Observador;

public class Controlador implements IControladorRemoto {
	private IVista vista;
	
	private IToDo modelo;

	public Controlador(IVista vista) {
		super();
		this.vista = vista;
		this.vista.setControlador(this);
		this.vista.mostrarMenu();
	}
	
	public void agregarTarea(String titulo, String descripcion) {
		try {
			this.modelo.agregarTarea(titulo, descripcion);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void listarTareas() {
		try {
			this.vista.listarTareas(this.modelo.listar());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	@Override
	public void actualizar(IObservableRemoto arg0, Object objeto) throws RemoteException {
		Eventos evento = (Eventos) objeto;
		switch(evento) {
			case TAREA_AGREGADA:
				this.listarTareas();
				break;
		}
	}

	@Override
	public <T extends IObservableRemoto> void setModeloRemoto(T modeloRemoto) throws RemoteException {
		this.modelo = (IToDo) modeloRemoto; // es necesario castear el modelo remoto 
	}	
}
