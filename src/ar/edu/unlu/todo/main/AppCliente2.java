package ar.edu.unlu.todo.main;

import java.rmi.RemoteException;

import ar.edu.unlu.rmimvc.RMIMVCException;
import ar.edu.unlu.rmimvc.cliente.Cliente;
import ar.edu.unlu.todo.controlador.Controlador;
import ar.edu.unlu.todo.modelo.ToDo;
import ar.edu.unlu.todo.vista.IVista;
import ar.edu.unlu.todo.vista.VistaConsolaSwing;

public class AppCliente2 {

	public static void main(String[] args) {
		IVista vista = new VistaConsolaSwing();
		Controlador controlador = new Controlador(vista);
		Cliente cliente = new Cliente("127.0.0.1", 9999, "127.0.0.1", 8888);
		try {
			cliente.iniciar(controlador); // enlaza el controlador con el modelo remoto 
		} catch (RemoteException e) {
			// error de conexión
		} catch (RMIMVCException e) {
			// error al crear el objeto de acceso remoto del modelo o del controlador 
		}
	}

}
