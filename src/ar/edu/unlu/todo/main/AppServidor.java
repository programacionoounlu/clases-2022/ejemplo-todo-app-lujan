package ar.edu.unlu.todo.main;

import java.rmi.RemoteException;

import ar.edu.unlu.rmimvc.RMIMVCException;
import ar.edu.unlu.rmimvc.servidor.Servidor;
import ar.edu.unlu.todo.modelo.ToDo;

public class AppServidor {

	public static void main(String[] args) {
		ToDo modelo = new ToDo(); // modelo
		Servidor servidor = new Servidor("127.0.0.1", 8888);
		try {
			servidor.iniciar(modelo);
		} catch (RemoteException e) {
			// error de conexión
		} catch (RMIMVCException e) {
			// error al crear el objeto de acceso remoto del modelo 
		}
	}

}
