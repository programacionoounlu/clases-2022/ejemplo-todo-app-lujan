package ar.edu.unlu.todo.main;
import ar.edu.unlu.todo.controlador.Controlador;
import ar.edu.unlu.todo.modelo.ToDo;
import ar.edu.unlu.todo.vista.IVista;
import ar.edu.unlu.todo.vista.VistaConsola;
import ar.edu.unlu.todo.vista.VistaConsolaSwing;

public class ToDoApp {

	public static void main(String[] args) {
		ToDo modelo = new ToDo();
		IVista vista = new VistaConsolaSwing();
		Controlador controlador = new Controlador(vista, modelo);
	}

}
