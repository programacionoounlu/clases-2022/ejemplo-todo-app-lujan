package ar.edu.unlu.todo.modelo;

import java.io.Serializable;
import java.util.Objects;

public class Tarea implements ITarea, Serializable {
	private static final long serialVersionUID = -5592696148939540110L;

	private boolean completada;
	
	private String titulo;
	
	private String descripcion;
	
	public Tarea(String titulo, String descripcion) {
		super();
		this.titulo = titulo;
		this.descripcion = descripcion;
		this.completada = false;
	}

	public boolean isCompletada() {
		return completada;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	public void completar() {
		this.completada = true;
	}

	@Override
	public int hashCode() {
		return Objects.hash(completada, descripcion, titulo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tarea other = (Tarea) obj;
		return completada == other.completada && Objects.equals(descripcion, other.descripcion)
				&& Objects.equals(titulo, other.titulo);
	}
	
	
		
}
