package ar.edu.unlu.todo.modelo;

import java.rmi.RemoteException;
import java.util.List;

import ar.edu.unlu.rmimvc.observer.IObservableRemoto;

public interface IToDo extends IObservableRemoto {

	void agregarTarea(String titulo, String descripcion) throws RemoteException;

	ITarea buscarTarea(String titulo) throws RemoteException;

	List<ITarea> listar() throws RemoteException;

}