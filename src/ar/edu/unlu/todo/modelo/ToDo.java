package ar.edu.unlu.todo.modelo;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ar.edu.unlu.rmimvc.observer.ObservableRemoto;
import ar.edu.unlu.utils.Observable;
import ar.edu.unlu.utils.Observador;

public class ToDo extends ObservableRemoto implements IToDo {
	
	private List<ITarea> tareas;
	
	
	public ToDo() {
		this.tareas = new ArrayList<>();
	}
	
	@Override
	public void agregarTarea(String titulo, String descripcion) throws RemoteException {
		this.tareas.add(new Tarea(titulo, descripcion));
		try {
			this.notificarObservadores(Eventos.TAREA_AGREGADA);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//public Tarea eliminarTarea(String titulo) {
		
	//}
	
	@Override
	public ITarea buscarTarea(String titulo) throws RemoteException {
		ITarea resultado = null;
		Iterator<ITarea> iterador = this.tareas.iterator();
		while(resultado == null && iterador.hasNext()) {
			ITarea tarea = iterador.next();
			if(tarea.getTitulo().toLowerCase().equals(titulo.toLowerCase())) {
				resultado = tarea;
			}
		}
		return resultado;
	}	
	
	@Override
	public List<ITarea> listar() throws RemoteException {
		return this.tareas;
	}

}
