package ar.edu.unlu.todo.modelo;

public interface ITarea {
	public boolean isCompletada();

	public String getTitulo();

	public String getDescripcion();
}
