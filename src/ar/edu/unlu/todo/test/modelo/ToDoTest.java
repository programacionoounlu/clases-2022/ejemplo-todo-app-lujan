package ar.edu.unlu.todo.test.modelo;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.unlu.todo.modelo.ITarea;
import ar.edu.unlu.todo.modelo.IToDo;
import ar.edu.unlu.todo.modelo.Tarea;
import ar.edu.unlu.todo.modelo.ToDo;

class ToDoTest {
	
	private IToDo todo;
	
	@BeforeEach
	public void beforeEach() {
		this.todo = new ToDo();
	}

	@Test
	void testAgregarTarea() {
		String titulo = "prueba 1";
		String descripcion = "descripción";
		this.todo.agregarTarea(titulo, descripcion);
		ITarea resultado = this.todo.buscarTarea("Prueba 1");
		assertEquals(new Tarea(titulo, descripcion), resultado);
	}

}
