package ar.edu.unlu.todo.vista;

import java.util.List;

import ar.edu.unlu.todo.controlador.Controlador;
import ar.edu.unlu.todo.modelo.ITarea;

public interface IVista {
	public void mostrarTarea(ITarea tarea);
	
	public void mostrarFormularioAlta();
	
	public void mostrarMenu();
	
	public void setControlador(Controlador controlador);

	public void listarTareas(List<ITarea> listar);
}
