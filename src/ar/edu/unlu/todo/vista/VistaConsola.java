package ar.edu.unlu.todo.vista;

import java.util.List;
import java.util.Scanner;

import ar.edu.unlu.todo.controlador.Controlador;
import ar.edu.unlu.todo.modelo.ITarea;

public class VistaConsola implements IVista {
	
	private Scanner entrada;
	
	private Controlador controlador;
	
	public VistaConsola() {
		this.entrada = new Scanner(System.in);
	}

	@Override
	public void mostrarTarea(ITarea tarea) {
		System.out.println("-------------------------------");
		System.out.println("TÍTULO: " + tarea.getTitulo());
		System.out.println();
		System.out.println("DESCRIPCIÓN:");
		System.out.println(tarea.getDescripcion());
		System.out.println();
		System.out.println("ESTADO:" + (tarea.isCompletada() ? "Completada" : "Pendiente"));
		System.out.println("-------------------------------");
	}

	@Override
	public void mostrarFormularioAlta() {
		System.out.println("-------------------------------");
		System.out.println("Alta de tarea");
		System.out.println("Título:");
		String titulo = this.entrada.nextLine();
		System.out.println("Descripción:");
		String descripción = this.entrada.nextLine();
		this.controlador.agregarTarea(titulo, descripción);
	}

	@Override
	public void mostrarMenu() {
		String opcion;		
		boolean salir = false;
		do {
			System.out.println("-------------------------------");
			System.out.println("---------- ToDo App -----------");
			System.out.println("-------------------------------");
			System.out.println();
			System.out.println("a - Agrega tarea");
			System.out.println("l - Listar tareas");
			System.out.println();
			System.out.println("s - Salir");			
			System.out.println("Opción?: ");
			opcion = this.entrada.nextLine();
			switch(opcion) {
				case "a":
					this.mostrarFormularioAlta();
					break;
				case "l":
					this.controlador.listarTareas();
					break;
				case "s":
					System.out.println("No olvide completar sus tareas!");
					salir = true;
					break;
				default: 
					System.out.println("Opción no válida");
			}			
		}while(!salir);
	}
	
	private boolean opcionValida(String opcion, String ...valoresPosibles){
		boolean respuesta = false;
		for(String s : valoresPosibles) {
			if(s.equals(opcion))
				respuesta = true;
		}
		return respuesta;
		
	}

	@Override
	public void setControlador(Controlador controlador) {
		this.controlador = controlador;		
	}

	@Override
	public void listarTareas(List<ITarea> listar) {
		System.out.println("Tareas:");
		for (ITarea iTarea : listar) {
			this.mostrarTarea(iTarea);
		}			
	}

}
