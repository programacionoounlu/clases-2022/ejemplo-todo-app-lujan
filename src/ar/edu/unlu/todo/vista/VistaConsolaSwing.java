package ar.edu.unlu.todo.vista;

import java.awt.EventQueue;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import ar.edu.unlu.todo.controlador.Controlador;
import ar.edu.unlu.todo.modelo.ITarea;

import com.jgoodies.forms.layout.FormSpecs;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VistaConsolaSwing extends JFrame implements IVista  {

	private JPanel contentPane;
	private JTextArea txtHistorico;
	private JButton btnEnter;
	private JTextField txtInput;
	
	private Controlador controlador;
	
	private String tituloActual;
	private Estados estadoActual = Estados.MENU;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					VistaConsolaSwing frame = new VistaConsolaSwing();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
	
	public void println(String texto) {
		txtHistorico.append(texto + "\n");
	}
	
	public void println() {
		println("");
	}
	

	/**
	 * Create the frame.
	 */
	public VistaConsolaSwing() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("default:grow"),
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,},
			new RowSpec[] {
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,}));
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, "1, 2, 4, 1, fill, fill");
		
		txtHistorico = new JTextArea();
		scrollPane.setViewportView(txtHistorico);
		
		txtInput = new JTextField();
		contentPane.add(txtInput, "1, 4, fill, default");
		txtInput.setColumns(10);
		
		btnEnter = new JButton("Ingresar");
		btnEnter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				handleClick();
			}
		});
		contentPane.add(btnEnter, "3, 4");
		setVisible(true);
	}
	
	private void handleClick() {
		if (estadoActual == Estados.ALTA_TAREA) {
			if (tituloActual == null) {
				tituloActual = txtInput.getText();
				println("Descripción:");
			} else {
				controlador.agregarTarea(tituloActual, txtInput.getText());
				tituloActual = null;
				estadoActual = Estados.MENU;
				mostrarMenu();
			}
		} else if (estadoActual == Estados.MENU) {
			switch (txtInput.getText().trim()) {
			case "a":
				this.mostrarFormularioAlta();
				break;
			case "l":
				this.controlador.listarTareas();
				break;
			case "s":
				break;
			default: 
				println("Opción no válida");
			}
		}
		txtInput.setText("");
	}

	@Override
	public void mostrarTarea(ITarea tarea) {
		println("-------------------------------");
		println("TÍTULO: " + tarea.getTitulo());
		println();
		println("DESCRIPCIÓN:");
		println(tarea.getDescripcion());
		println();
		println("ESTADO:" + (tarea.isCompletada() ? "Completada" : "Pendiente"));
		println("-------------------------------");
	}

	@Override
	public void mostrarFormularioAlta() {
		estadoActual = Estados.ALTA_TAREA;
		println("-------------------------------");
		println("Alta de tarea");
		println("Título:");
	}

	@Override
	public void mostrarMenu() {
		println("-------------------------------");
		println("---------- ToDo App -----------");
		println("-------------------------------");
		println();
		println("a - Agrega tarea");
		println("l - Listar tareas");
		println();
		println("s - Salir");			
		println("Opción?: ");
	}

	@Override
	public void setControlador(Controlador controlador) {
		this.controlador = controlador;
	}

	@Override
	public void listarTareas(List<ITarea> listar) {
		println("Tareas:");
		for (ITarea iTarea : listar) {
			this.mostrarTarea(iTarea);
		}
	}

}
